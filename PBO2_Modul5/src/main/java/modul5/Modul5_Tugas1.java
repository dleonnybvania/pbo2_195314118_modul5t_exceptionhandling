/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul5;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTextField;

public class Modul5_Tugas1 extends JFrame implements ActionListener{
    private static final int FRAME_WIDTH    = 500;
    private static final int FRAME_HEIGHT   = 200;
    private static final int FRAME_X_ORIGIN = 700;
    private static final int FRAME_Y_ORIGIN = 350;
    private JButton button;
    private JTextField p, l, Lu;
    private JLabel labelPanjang;
    private JLabel labelLebar;
    private JLabel labelLuas;
    
    public static void main(String[] args) {
        Modul5_Tugas1 frame = new Modul5_Tugas1();
        frame.setVisible(true);
    }
    
    public Modul5_Tugas1(){

        Container contentPane = getContentPane( );
        contentPane.setLayout(null);

        //set the frame properties
        setSize      ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable ( false );
        setTitle     ( "Program Exception Handling Tugas 1" );
        setLocation  ( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        
        labelPanjang = new JLabel("Panjang (m)");
        labelPanjang.setBounds(35, 20, 95, 40);
        contentPane.add(labelPanjang);
        
        labelLebar = new JLabel("Lebar (m)");
        labelLebar.setBounds(35, 60, 95, 40);
        contentPane.add(labelLebar);
        
        labelLuas = new JLabel("Luas (m2)");
        labelLuas.setBounds(35, 80, 95, 40);
        contentPane.add(labelLuas);
        
        p = new JTextField();
        p.setBounds(200, 15, 320, 20);
        contentPane.add(p);
        
        l = new JTextField();
        l.setBounds(200, 45, 320, 20);
        contentPane.add(l);
        
        Lu = new JTextField();
        Lu.setBounds(200, 75, 320, 20);
        contentPane.add(Lu);
        
        button = new JButton("Hitung");
        button.setBounds(100, 130, 80, 20);
        contentPane.add(button);
        
        //registering a ButtonHandler as an action listener of the two buttons
        
        button.addActionListener(this);
        
        
        setDefaultCloseOperation( EXIT_ON_CLOSE );
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        int bil1, bil2;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try{
        JButton buttonText = (JButton) e.getSource();
        
        JRootPane rootPane = buttonText.getRootPane( );
        Frame     frame    = (JFrame) rootPane.getParent();
        
        bil1 = Integer.parseInt(p.getText());
        bil2 = Integer.parseInt(l.getText());
        
        if(e.getSource() == button){
            Lu.setText(" " + (bil1*bil2));
            frame.setTitle("Luas Tanah");
        } else{
            frame.setTitle("You clicked " + buttonText);
        }
          
        }
        
        catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "Maaf "
            + "hanya integer yang diperbolehkan!"
            , "Error", JOptionPane.ERROR_MESSAGE);
            
        }
    }
    
}



